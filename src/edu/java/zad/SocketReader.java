package edu.java.zad;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketReader extends Thread {

	Socket socket = null;
        String name = null;

	public SocketReader(Socket socket, String name) {
		super();
		this.socket = socket;
                this.name = name;
	}

	@Override
	public void run() {
		try {
			BufferedReader socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			while (true) {
				String temp;
				while ((temp = socketIn.readLine()) != null)
					System.out.println("user: "+ name +socket.getInetAddress() + " : " + temp);
				if (temp.equals("exit")) {
					socketIn.close();
					break;
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
