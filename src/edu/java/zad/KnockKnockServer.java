package edu.java.zad;

import java.net.*;
import java.io.*;
import java.util.Scanner;


public class KnockKnockServer {
	public static void main(String[] args) throws IOException {
		
		ServerSocket serverSocket = null;
		
		try {
			serverSocket = new ServerSocket(6000);
		} catch (IOException e) {
			System.err.println("Could not listen on port: 4444.");
			System.exit(1);
		}
		
		Socket clientSocket = null;
		
		try {
			clientSocket = serverSocket.accept();
		} catch (IOException e) {
			System.err.println("Accept failed.");
			System.exit(1);
		}
		
		String name= args[0];
//		Scanner input = new Scanner(System.in);
//		System.out.println("Write Your name: ");	  
//		name = input.nextLine();
		
		SocketWriter pisarz = new SocketWriter(clientSocket, name);
		SocketReader czytelnik = new SocketReader(clientSocket, "Bartek");
		
		
		
		pisarz.start();
		czytelnik.start();
		
		
		//serverSocket.close();
		//clientSocket.close();
	}
}
