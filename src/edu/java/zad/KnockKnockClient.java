package edu.java.zad;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class KnockKnockClient {
	public static void main(String[] args) throws IOException {
		Socket kkSocket = null;
		PrintStream out = null;
		DataInputStream in = null;
		try {
			kkSocket = new Socket(InetAddress.getLocalHost(), 6000);
			out = new PrintStream(kkSocket.getOutputStream());
			in = new DataInputStream(kkSocket.getInputStream());
                        
		} catch (UnknownHostException e) {
			System.err.println("I cant find host");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Cannot find connection with host");
			System.exit(1);
		}
		
		String name= args[0];
//		Scanner input = new Scanner(System.in);
//		System.out.println("Write Your name: ");	  
//		name = input.nextLine();
		
		SocketWriter pisarz = new SocketWriter(kkSocket, name);
		SocketReader czytelnik = new SocketReader(kkSocket, "Marek");
		
		pisarz.start();
		czytelnik.start();
		
		//kkSocket.close();
	}
}
