package edu.java.zad;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

public class SocketWriter extends Thread {

	Socket socket = null;
        String name = null;

	public SocketWriter(Socket socket, String name) {
		super();
		this.socket = socket;
                this.name = name;
	}

	@Override
	public void run() {
		try {
			DataInputStream stdIn = new DataInputStream(System.in);
			PrintStream out = new PrintStream(new BufferedOutputStream(socket.getOutputStream(), 1024), false);
			out.flush();
			while (true) {
				String temp;
				while ((temp = stdIn.readLine()) != null)
				{
					System.out.println("user: "+ name +"("+InetAddress.getLocalHost()+ ")"  +temp);
					out.println(temp);
					out.flush();
				}
				if (temp.equals("exit")) {
					stdIn.close();
					break;
				}
			
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}