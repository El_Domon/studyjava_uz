package binary.tree;

public class BinaryTree {
	private BinaryTree leftNode = null;
	private BinaryTree rightNode = null;
	private Integer value = null;
	private int nodeId = 1;

	public void send(Integer value) {
		if (this.value == null)
			this.value = value;
		else if (value < this.value) {
			this.leftNode = new BinaryTree();
			this.leftNode.send(value);
			this.leftNode.nodeId = this.nodeId++;
		} else if (value >= this.value) {
			this.rightNode = new BinaryTree();
			this.rightNode.send(value);
			this.rightNode.nodeId = this.nodeId++;
		} else
			System.out.println("CRITAL ERROR!");

	}

	public Integer read() {
		return this.value;
	}

	public Integer search(Integer searchedValue) {
		Integer childValue = 0;

		if (searchedValue == this.value) {
			return this.nodeId;
		}
		if (searchedValue < this.value) {
			System.out.println("Mniejsze  | W�z�: " + nodeId + "| Warto��: " + this.value + "| ChildValue:" + childValue);
			if (this.leftNode == null)
				return null;
			else
				childValue = this.leftNode.search(searchedValue);
		}
		if (searchedValue > this.value) {
			System.out.println("Wieksze  | W�z�: " + nodeId + "| Warto��: " + this.value + "| ChildValue:" + childValue);
			if (this.rightNode == null)
				return null;
			else
				childValue = this.rightNode.search(searchedValue);
		}

		if (childValue == searchedValue) {
			System.out.println("END: " + nodeId + " | ChildValue:" + childValue);
			return childValue;
		}

		return 155;
	}
}
